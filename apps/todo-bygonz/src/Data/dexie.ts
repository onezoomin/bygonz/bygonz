import { BygonzDexie, DexiePlus, DexiePlusParams } from 'bygonz'
import { Table } from 'dexie'
import { CompoundKeyNumStr, TaskVM } from '../Model/Task'
import { initialActiveTasks, initialCompletedTasks } from '../Model/Tasks'
import { TaskParams } from './../Model/Task'
// import { DexiePlus } from './DexiePlus'

// const Dexie = await getDexieFromCDN()
// const Dexie = (await import('https://unpkg.com/dexie@3.2.1/dist/modern/dexie.mjs')).Dexie
// const Dexie = (await import('https://unpkg.com/dexie@3.2.1/dist/dexie.mjs')).Dexie
// const Dexie = (await import('dexie')).Dexie // reactive
const Dexie = globalThis.Dexie // reactive
console.log('Dexie', Dexie)
console.log('DexiePlus', DexiePlus)
console.log('BygonzDexie', BygonzDexie)

const stores = {
  ActiveTasks: '[created+owner], created, modified, owner',
  CompletedTasks: '[created+owner], created, modified, owner',
}

const mappings = {
  ActiveTasks: TaskVM,
  CompletedTasks: TaskVM,
}
const options = {
  conflictThresholds: {
    red: 120,
    yellow: 600,
  },
  conflictHandlers: {},
}

export class TodoDB extends BygonzDexie {
  // Declare implicit table properties. (just to inform Typescript. Instanciated by Dexie in stores() method)
  // TaskParams | TaskVM allows for partial objects to be used in add and put and for the class to include getters
  public ActiveTasks: Table<TaskParams | TaskVM, CompoundKeyNumStr>// CompoundKeyNumStr = type of the priKey
  public CompletedTasks: Table<TaskParams | TaskVM, CompoundKeyNumStr> // TODO get rid of this weird assignment  = this._allTables.ActiveTasks

  async init () {
    // TODO consider populate https://dexie.org/docs/Dexie/Dexie.on.populate
    const at = this.ActiveTasks
    const ct = this.CompletedTasks

    if (at && (await at.count()) === 0) {
      await at.bulkAdd(initialActiveTasks)
    }
    if (ct && (await ct.count()) === 0) {
      await ct.bulkAdd(initialCompletedTasks)
    }
  }

  constructor (...params: DexiePlusParams) {
    super(...params)
    this.doMappings()
    // super(params[0]) // reactivity works if extending Dexie (not loaded from CDN) and using these normal instantiations
    // this.version(1).stores(stores)
  }
}

let todoDB
export const getInitializedTodoDB = async () => {
  if (todoDB) return todoDB
  todoDB = new TodoDB('Todo', stores, mappings, options)
  await todoDB.init()
  return todoDB
}
