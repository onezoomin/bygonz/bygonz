import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import { IconButton } from '@mui/material'
import { getBrocastChannelSubscribedTo } from 'dexie-preact-hooks'
import { useLiveQuery } from 'dexie-react-hooks'
import React from 'preact/compat'
import { useEffect, useRef, useState } from 'preact/hooks'
import { ActiveTasksQuery, completeActiveTask, delActiveTask, ModificationsQuery, updateActiveTask } from '../../Data/data'
import { Task, TaskVM } from '../../Model/Task'
import Editable from '../Editable'

// const  {useLiveQuery}  = await import( 'https://npmcdn.com/dexie-react-hooks@1.1.1/dist/dexie-react-hooks.js')

console.log('preHooks default', useLiveQuery)

// const {useLiveQuery} = pHooks
function TaskMods ({ task: { id } }) {
  const mods = useLiveQuery(() => ModificationsQuery(id)) ?? []
  return (
    !mods.length ? null : <div>{mods.length}</div>
  )
}
let bc
export default function ActiveTasks () {
  const [isDirty, setIsDirty] = useState(false)
  const [isListening, setIsListening] = useState(false)
  const toggleIsDirty = () => setIsDirty(!isDirty)
  // const [since, setSince] = useState(utcMsTs())
  // const activeTasks = useObservable<TaskVM[]>(liveQuery(ActiveTasksQuery)) ?? [] // constant rerender loop
  // const newActiveTasks = useLiveQuery(() => ActiveTasksSinceQuery(utcMsTs()), [], [])

  useEffect(() => {
    const handleSubscription = (ev) => {
      console.log('bc event', ev)

      // toggleIsDirty()
    }
    if (!bc) {
      bc = getBrocastChannelSubscribedTo(handleSubscription)
      console.log('new bc', bc)
      setIsListening(true)
    } else {
      console.log('useEffect ran with old bc should only happen once', bc)
    }
  }, [isListening])
  const activeTasks = useLiveQuery(ActiveTasksQuery, [], [])
  console.log('rendering', activeTasks.length, activeTasks[0])
  // isDirty && setIsDirty(false)

  const onCheck = (checkedTask: Task) => {
    console.log(checkedTask)
    void completeActiveTask(checkedTask)
  }
  const onDelete = (deletedTask: TaskVM) => {
    void delActiveTask(deletedTask.id)
    // setIsDirty(!isDirty)
  }
  const inputRef = useRef<any>()

  async function updateTask (task: Task, newVal) {
    task.task = newVal
    await updateActiveTask(task)
    // setIsDirty(!isDirty)
  }
  // setTimeout(() => { setIsDirty(true) }, 1500)
  return (
    <div class="container overflow-y:auto mx-auto mb-5">
      {activeTasks?.map((task: TaskVM, i) => {
        const id = task.id ?? i
        return (
          <div key={id} class="flex flex-wrap px-5 md:px-20">
            <IconButton onClick={() => onCheck(task)}>
              <CheckBoxOutlineBlankIcon />
            </IconButton>

            <Editable
                class="flex-grow w-2/3"
                text={task.task}
                placeholder="Write a task name"
                type="input"
                childRef={inputRef}
                onEnter = {(e: KeyboardEvent) => { void updateTask(task, (e.target as HTMLInputElement)?.value) }}
            />
            {/* <TaskMods {...{ task }} /> */}
            <IconButton onClick={() => onDelete(task)}>
              <DeleteForeverIcon />
            </IconButton>
          </div>
        )
      })}
    </div>
  )
}
