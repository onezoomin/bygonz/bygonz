# Bygonz
### Remember everything, until you don't want to.

local first,client first offline first, far edge

inspired by datalog, blockchain, webnative

Bygonz aims to provide fast reliable data flow solutions especially for edge deployed Web apps.

 - datomic style datalog entries
 - datalog merging
 - conflict flagging system
    - yellow flag - merged into the timeline but flagged for review
    - red flag - rebased only after conscious confirmation
 - epoch roll ups - zipped and signed by all trusted collaborato

### configurable 
 - epoch lengths
 - flag rules
 - persistence


