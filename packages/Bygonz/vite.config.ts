// import resolve from '@rollup/plugin-node-resolve'
import path from 'path'
import { defineConfig } from 'vite'
import { devDependencies, peerDependencies } from './package.json'
// import worker, { pluginHelper } from 'vite-plugin-worker'
export default defineConfig({
  // optimizeDeps: {
  //   include: ['dexie'],

  // },
  // plugins: [
  //   pluginHelper(),
  //   worker({}),
  // ],
  worker: {
    format: 'es',
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      // inlineDynamicImports: true, // didn't help infinite loop
      external: [...Object.keys(peerDependencies), ...Object.keys(devDependencies)],
    },
  },
  // resolve: {
  //   dedupe: [...Object.keys(peerDependencies), ...Object.keys(devDependencies)],
  // },
  build: {
    target: 'esnext',
    sourcemap: false,
    minify: false,
    watch: {
      include: 'src/**/*',
    },
    // commonjsOptions: {
    //   // transformMixedEsModules: true,
    //   dynamicRequireTargets: [
    //     'src/BygonzWebWorker.ts',
    //   ],
    // },
    lib: {
      formats: ['es'],
      entry: path.resolve(__dirname, 'index.ts'),
      name: 'bygonz',
      fileName: (format) => `bygonz.${format}.js`,

    },

    rollupOptions: {
      watch: {
        include: 'src/**/*',
      },
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      // inlineDynamicImports: true, // didn't help infinite loop
      // external: [...Object.keys(peerDependencies), ...Object.keys(devDependencies)],
      // plugins: [resolve()],
      // output: {
      //   // Provide global variables to use in the UMD build
      //   // for externalized deps
      //   globals: {
      //     vue: 'Vue',
      //   },
      // },
    },
  },
  // define: {
  //   'process.env': process?.env || { isDev: import.meta.env.DEV }, // needed in addition to nodePolyfills
  //   // 'globalThis.Buffer': Buffer, // maybe needed in addition to nodePolyfills
  //   // Buffer, // maybe needed in addition to nodePolyfills
  // },
})
