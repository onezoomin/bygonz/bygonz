import { getDexiePlusCDN } from './WebWorkerImports/DexiePlusCDN'
import { dgraphMod, fetchAndApplyMods } from './WebWorkerImports/dgraph-socket'
import { getModDB } from './WebWorkerImports/Mods'
import { BYGONZ_MUTATION_EVENT_NAME, checkWorker, utcMsTs } from './WebWorkerImports/Utils'
// checkWorker('top of bygonz worker')

// hack to avoid overlay.ts's dom assumptions
self.HTMLElement = function () {
  return {}
}

self.customElements = {
  get () {
    return []
  },
}

let targetDB
export let modDB
self.onmessage = async (e) => {
  if (e.data.cmd === 'init') {
    checkWorker('init')
    if (!targetDB) {
      const { dbName, stores, options } = e.data
      const DexiePlusCDN = await getDexiePlusCDN()

      if (!(await DexiePlusCDN.exists(dbName))) {
        console.log('RED FLAG - DB does not exist')
      }
      const unmappedDB = new DexiePlusCDN(dbName, stores, options)

      targetDB = unmappedDB
      checkWorker('just opened ->', targetDB)
    } else {
      checkWorker('already opened ->', targetDB)
    }
    if (!modDB) {
      // const { getModDB } = await import('./WebWorkerImports/Mods')

      modDB = await getModDB()
      checkWorker('just opened ->', modDB)
    } else {
      checkWorker('already opened ->', modDB)
    }

    // const { dgraphMod, fetchAndApplyMods } = await import('./WebWorkerImports/dgraph-socket')
    const modTable = modDB.Mods

    const commitMod = async (modLogEntry) => {
      await modTable.put(modLogEntry)
      await dgraphMod(modLogEntry)
      await fetchAndApplyMods(modDB, targetDB)
    }

    void fetchAndApplyMods(modDB, targetDB, true)

    // https://github.com/dexie/Dexie.js/blob/master/src/globals/global-events.ts
    // const DEXIE_STORAGE_MUTATED_EVENT_NAME = 'storagemutated' as 'storagemutated'
    // const STORAGE_MUTATED_DOM_EVENT_NAME = 'x-storagemutated-1'

    // https://github.com/dexie/Dexie.js/blob/master/src/live-query/enable-broadcast.ts
    const bc = new BroadcastChannel(BYGONZ_MUTATION_EVENT_NAME)

    // targetDB.on('storagemutated', (ev) => {
    //   console.log('storagemutated', ev)
    // })

    bc.onmessage = (ev) => {
      // if (ev.data) propagateLocally(ev.data);
      checkWorker('bygonz afterEffect', ev, utcMsTs())

      const { data: modEntry } = ev
      void commitMod(modEntry)
    }

    // https://isomorphic-git.org/docs/en/webworker
    // const MagicPortal = await import('https://unpkg.com/magic-portal')
    // const gitWorker = new Worker('./WebWorkerImports/git-worker.js')
    // const portal = new MagicPortal(gitWorker)
    // gitWorker.addEventListener('message', ({ data }) => console.log(data))
    // gitWorker.postMessage('init')
  } // </init
}
