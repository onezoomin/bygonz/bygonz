// import { DexiePlusParams } from './WebWorkerImports/DexiePlus';
// import { Dexie } from 'dexie'
// const Dexie = (await import('dexie')).Dexie // results in repackaging of dexie in dist
// const Dexie = (await import('https://unpkg.com/dexie@3.2.1/dist/dexie.mjs')).Dexie

// import { Dexie } from 'dexie' // works
const Dexie = globalThis.Dexie // not if from cdn but yes from import { Dexie } from 'dexie'

const defaultOptions = {
  conflictThresholds: {
    red: 60,
    yellow: 300,
  },
}

export class DexiePlus extends Dexie {
  public workerApi
  public stores
  public mappings
  public options

  public onWorkerMsg = console.log

  protected doMappings () {
    // console.log('befor tables?', this)
    for (const eachTable of this.tables) {
      // console.log('before constructor table assignment', this[eachTable.name])
      this[eachTable.name] = eachTable // ensure there is a reference to each table "on" the db instance
      // console.log('after constructor table assignment', this[eachTable.name], this)

      if (this.mappings[eachTable.name]) {
        // console.log(this[eachTable.name], 'mapping', eachTable, 'to', mappings[eachTable.name])
        eachTable.mapToClass(this.mappings[eachTable.name])
      }
    }
    // console.log('after tables?', this)
  }

  constructor (name: string, stores: Record<string, string>, mappings: Record<string, any> = {}, options = defaultOptions, version = 1) {
    if (!name || !stores) throw new Error('DexiePlus requires params (name, {stores})')
    super(name)

    this.version(version).stores(stores)

    this.options = options
    this.stores = stores
    this.mappings = mappings
    // this.doMappings() // needs to be called in the final child class that extends BygonzDexie
  }
}
