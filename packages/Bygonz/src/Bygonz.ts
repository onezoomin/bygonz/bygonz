import { getBygonzMiddlwareFor } from './BygonzMiddleware'
import BygonzWorker from './BygonzWebWorker?worker' // vite native  // &inline
import { DexiePlus } from './DexiePlus'
import { DexiePlusParams } from './WebWorkerImports/DexiePlusCDN'

export class BygonzDexie extends DexiePlus {
  async spawnWorker () {
    console.log('spawning')

    if (self.document === undefined) { return console.log('avoiding infinite loop by not setting up worker if not in main ui thread') }

    // const { default: BygonzWorker } = await import('./BygonzWebWorker.ts?worker&inline')
    // this.workerApi = new Worker(BygonzWorkerURL, { type: 'module' })
    this.workerApi = new BygonzWorker()
    this.workerApi.onmessage = this.onWorkerMsg
    this.workerApi.postMessage({ cmd: 'init', dbName: this.name, stores: this.stores, options: this.options }) // init
    console.log('spawned', this.workerApi)
  }

  constructor (...params: DexiePlusParams) {
    const [dbName, ...restParams] = params
    super(`${dbName}_BygonzDexie`, ...restParams)
    // doMappings() should be called by the end consumer constructor
    if (self.document !== undefined) {
      console.log('ui side, setting up middleware and spawning worker')
      this.use(getBygonzMiddlwareFor(this))
      void this.spawnWorker()
    }
  }
}

const notes = `

https://github.com/dexie/Dexie.js/blob/master/src/live-query/observability-middleware.ts
https://github.com/dexie/Dexie.js/blame/master/src/hooks/hooks-middleware.ts
https://github.com/dexie/Dexie.js/blob/master/src/live-query/live-query.ts

https://github.com/dexie/Dexie.js/blob/master/src/live-query/enable-broadcast.ts

`
