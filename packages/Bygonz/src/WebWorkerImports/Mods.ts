// import { Dexie } from 'dexie'
import { getDexieFromCDN } from './DexiePlusCDN'
import { utcMsTs } from './Utils'
// const { Dexie } = await import('https://unpkg.com/dexie@3.2.1/dist/modern/dexie.mjs')

export type CompoundKeyNumStr = [number, string]
export type ModCompoundKey = [number /* ts */, string /* modifier */, string /* op */, string /* tableName */]

export enum Operations {
  CREATE='C',
  READ='R',
  UPDATE='U',
  DELETE='D'
}
export const OpCodes = {
  C: 'CREATE',
  R: 'READ',
  U: 'UPDATE',
  D: 'DELETE',
}

// Dgraph graphql+ type
// type Mod {
//   key: String! @id
//   ts: Int64! @search
//   tableName: String! @search(by: [fulltext])
//   forKey: String! @search(by: [fulltext])
//   owner: String! @search(by: [fulltext])
//   modifier: String! @search(by: [fulltext])
//   op: OpCodes
//   log: String!
// }
export class TimeStampedBase {
  created: number = utcMsTs()
  modified: number = this.created

  constructor (obj: any) {
    Object.assign(this, obj)
  }
}

export class ModWho {
  owner: string
  modifier: string

  constructor (obj: any) {
    // super(obj)
    Object.assign(this, obj)
  }
}

export class ModObj<forKeyType> extends ModWho {
  ts: number
  tableName: string
  forKey: forKeyType
  op: Operations
  log: Record<any, any>
  constructor (obj: ModObj<forKeyType>) {
    super(obj)
    Object.assign(this, obj)
  }
}

export class ModVM extends ModObj<any> {
  static getCompoundKey (obj: ModObj<any>): ModCompoundKey {
    return [obj.ts, obj.modifier, obj.op, obj.tableName]
  }

  public forGql (): Record<any, any> { // TODO create type for jsonified mod
    const mod: Record<any, any> = { ...this }
    mod.key = this.gqlKey
    mod.log = JSON.stringify(this.log)
    mod.forKey = JSON.stringify(this.forKey)
    return mod
  }

  public get gqlKey (): string {
    return JSON.stringify(ModVM.getCompoundKey(this))
  }

  public get gqlForKey (): string {
    return JSON.stringify(this.forKey)
  }

  public get opString (): string {
    return OpCodes[this.op]
  }

  public get id (): ModCompoundKey {
    return ModVM.getCompoundKey(this)
  }
}
export const modStoresDef = {
  Mods: '[ts+modifier+op+tableName], [op+forKey], ts, tableName, forKey, op',
}

let modDB
export const getModDB = async () => {
  if (modDB) return modDB

  const Dexie = await getDexieFromCDN()
  class ModDB extends Dexie {
    Mods: Dexie.Table<ModVM | ModObj<any>, ModCompoundKey>

    constructor () {
      super('Bygonz_ModDB')
      this.version(1).stores(modStoresDef)
      this.Mods.mapToClass(ModVM) //   https://dexie.org/docs/Typescript#storing-real-classes-instead-of-just-interfaces
    }
  }

  modDB = new ModDB()
  return modDB as ModDB
}
