import { ripemd160 } from '@ethersproject/sha2'
import format from 'date-fns/format'
import fromUnixTime from 'date-fns/fromUnixTime'
import getMinutes from 'date-fns/getMinutes'
import set from 'date-fns/set'
import type { Dexie } from 'dexie'
import { dump } from 'js-yaml'
import { getDexieFromCDN } from './DexiePlusCDN'
import { commitEpochToGit } from './git'
import { getModDB, type ModObj } from './Mods'
import { utcMsTs } from './Utils'

// TODO optimize date-fns https://cdn.skypack.dev/pin/date-fns@v2.28.0-zvghaYXqcEkTnsd3AFYD/mode=imports,min/optimized/date-fns.js
export class EpochObj {
  data: string
  hash?: string
  date?: string
  ts?: number

  constructor (params: EpochObj) {
    params.ts = params.ts ?? utcMsTs()
    params.date = format(params.ts, 'yyyy.MM.dd_H:mm')
    Object.assign(this, params)
  }
}
export class EpochClass extends EpochObj {
  declare ts: number
  public get hashCalc (): string {
    return ripemd160((new TextEncoder()).encode(this.data))
  }
}

let epochDB
export const getEpochDB = async () => {
  if (epochDB) return epochDB

  const Dexie = await getDexieFromCDN()
  class EpochDB extends Dexie {
    // [x: string]: any
    // Declare implicit table properties. (just to inform Typescript. Instanciated by Dexie in stores() method)
    Epochs: Dexie.Table<EpochClass | EpochObj, number> // number = type of the priKey
    // spanMs: number
    // ...other tables go here...

    async init () {
      console.log('init')
    }

    constructor () {
      super('Bygonz_EpochDB')

      this.version(1).stores({
        Epochs: 'ts, date',
      })

      // addTableRefs(this)
      this.Epochs.mapToClass(EpochClass)
    }
  }
  epochDB = new EpochDB()
  return epochDB as EpochDB
}
// void opLogRollup(true, true)
// const rollupInterval = setInterval(() => {
//   void opLogRollup()
// }, 10000)
// const minuteEpochDB = new EpochDB(6000, 'Minute')
// const minutesTable = minuteEpochDB.Epochs
// const modTable = modDB.Mods

const yamlOptions = {
  noArrayIndent: true,
}

export const createYamlLog = async (usePrevMinute = false) => {
  const modDB = await getModDB()
  const epochDB = await getEpochDB()
  const nownow = utcMsTs()
  const nowDate = fromUnixTime(nownow / 1000)
  const prevMinute = set(nowDate, { minutes: getMinutes(nowDate) - 1, seconds: 0, milliseconds: 0 }).getTime()
  const nextMinute = set(nowDate, { minutes: getMinutes(nowDate) + 1, seconds: 0, milliseconds: 0 }).getTime()
  const thisMinute = set(nowDate, { minutes: getMinutes(nowDate), seconds: 0, milliseconds: 0 }).getTime()
  const msUntilEpoch = nextMinute - nownow

  const minToUse = usePrevMinute ? prevMinute : thisMinute
  const modsForCurrentMinute = (await modDB.Mods.where('ts').above(minToUse).toArray())
  const mapped = modsForCurrentMinute.map(({ ts, tableName, op, forKey, log }: ModObj<any>) => {
    return {
      [`${format(ts, ':ss.SSS')}`]: {
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        [`${tableName}__${op}__${forKey.toString()}`]: log, // TODO ts
      },
    }
  })
  const yaml = dump({ [format(minToUse, 'H:mm')]: mapped }, yamlOptions)

  // if (msUntilEpoch < 10000) console.log('finish Epoch', minToUse)
  if (modsForCurrentMinute.length) {
    const newEpoch = new EpochClass({ ts: minToUse, data: yaml })
    if (usePrevMinute) {
      newEpoch.hash = newEpoch.hashCalc
      console.log('hash', newEpoch.hash)
      // await commitEpochToGit(`${minToUse}.${newEpoch.hash}`, newEpoch.data)
    }
    void commitEpochToGit(`${minToUse}.${newEpoch.hash ?? 'pending'}`, newEpoch.data)
    void epochDB.Epochs.put(newEpoch)
  }

  console.log(yaml, format(minToUse, 'H:mm:ss:SSS'), 'next epoch in', msUntilEpoch)
}

const createHashFromEpochEntry = (entry: string) => {
  console.log('cryptoHashOf', entry)
}

const EpochEntryExample = {
  '13:37': {
    ':10.905': {
      'ActiveTasks__U__1648557321841,0x49Dda9CD9A36B90B34412CbABf687d783DA0eaa4': {
        put:
        {
          task: '49dttt',
          modifier: '0xd51e897DC57A9318390584707E39970aDab1A70b',
          modified: 1648557430905,
        },
        obj:
        {
          created: 1648557321841,
          modified: 1648557430905,
          task: '49dttt',
          status: 'Active',
          owner: '0x49Dda9CD9A36B90B34412CbABf687d783DA0eaa4',
          modifier: '0xd51e897DC57A9318390584707E39970aDab1A70b',
        },
        revert:
        {
          task: '49d',
          modified: 1648557321841,
        },
      },
    },
  },

}
const signedEpochExample = {
  [`${Object.keys(EpochEntryExample)[0]}`]: {
    hash: createHashFromEpochEntry(dump(EpochEntryExample)), // should use yaml string (and ensure stable deterministic sorting)
    sigs: {
      '0c0x49d': 'sig0x0x0x',
    },
  },
}
