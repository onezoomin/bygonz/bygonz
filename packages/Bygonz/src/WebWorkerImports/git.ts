import type FST from '@isomorphic-git/lightning-fs'
import { type Buffer as BufferT } from 'buffer'
import type gitT from 'isomorphic-git' // import git from 'isomorphic-git'
import type stringifyT from 'json-stable-stringify'

const crudEx = { // {milliseconds_userid: {EVENT_TYPE: {Event:Data} } },
  '86400000_0x000': { C: { Event: 'Data' } },
  '85400000_0x000': { R: { Event: 'Data' } },
  '84400000_0x000': { U: { Event: 'Original Data' } },
  '83400000_0x000': { D: { Event: 'Data' } },
}
const defaultBranch = 'trunk'
let fs: FST
let git: typeof gitT
let BufferCDN: BufferT
let stringify: typeof stringifyT
export async function getGit () {
  console.log('gitInit', fs)
  if (!fs) {
    const FS = (await import('https://cdn.skypack.dev/pin/@isomorphic-git/lightning-fs@v4.6.0-ywW4nN92mAV6bKqa4gZl/mode=imports/optimized/@isomorphic-git/lightning-fs.js')).default
    console.log('FS for git', FS)
    fs = new FS('isogitfs')
    console.log('instantiated fs for git', fs)
  }
  if (!git) {
    git = (await import('https://cdn.skypack.dev/pin/isomorphic-git@v1.17.0-YDwGUjHvtBvcVmgeJ4wb/mode=imports/optimized/isomorphic-git.js')).default
    console.log('git for git', git)
  }
  if (!BufferCDN) {
    BufferCDN = (await import('https://cdn.skypack.dev/pin/buffer@v6.0.3-9TXtXoOPyENPVOx2wqZk/mode=imports/optimized/buffer.js')).default
    globalThis.Buffer = BufferCDN
    console.log('Buffer for git', globalThis.Buffer)
  }
  if (!stringify) {
    stringify = (await import('https://cdn.skypack.dev/pin/json-stable-stringify@v1.0.1-4k2xWeiI2f0VcCyOYEsW/mode=imports/optimized/json-stable-stringify.js')).default
    console.log('stringify for git', stringify)
  }

  await git.init({ fs, defaultBranch, dir: '/Epochs' })

  return git

  // console.log('survived git.init', fs)
  // await fs.promises.writeFile('/DailyEvents/22_123.json', stringify(crudEx, { space: 2 }))
  // await git.add({ fs, dir: '/DailyEvents', filepath: '22_123.json' })
  // const sha = await git.commit({ // eslint-disable-line @typescript-eslint/no-unused-vars
  //   fs,
  //   dir: '/DailyEvents',
  //   author: {
  //     name: 'Mr. Test',
  //     email: 'mrtest@example.com',
  //   },
  //   message: 'Added the 22_123.json file',
  // })
  // // console.log(sha, crudEx)
  // crudEx['84400000_0x000'].U.Event = 'Changed Data'
  // await fs.promises.writeFile('/DailyEvents/22_123.json', stringify(crudEx, { space: 2 }))

  // const status = await git.status({ fs, dir: '/DailyEvents', filepath: '22_123.json' })
  // console.log(status)

  // await git.add({ fs, dir: '/DailyEvents', filepath: '22_123.json' })

  // const sha2 = await git.commit({ // eslint-disable-line @typescript-eslint/no-unused-vars
  //   fs,
  //   dir: '/DailyEvents',
  //   author: {
  //     name: 'Mr. Test',
  //     email: 'mrtest@example.com',
  //   },
  //   message: 'Changed the 22_123.json file',
  // })
  // // console.log(sha2, await getFileStateChanges(sha, sha2, '/DailyEvents'))

  // const commits = await git.log({
  //   fs,
  //   dir: '/DailyEvents',
  //   depth: 4,
  // })

  // console.log('git commits', commits)

  // const after = Buffer.from((await git.readBlob({
  //   fs,
  //   dir: '/DailyEvents',
  //   oid: commits[0].oid,
  //   filepath: '22_123.json',
  // })).blob).toString('utf8')
  // const before = Buffer.from((await git.readBlob({
  //   fs,
  //   dir: '/DailyEvents',
  //   oid: commits[1].oid,
  //   filepath: '22_123.json',
  // })).blob).toString('utf8')

  // // console.log(blob, blob2)
  // // console.log(diffLines(blob, blob2))
  // let sylyableDiff = diff(before, after).replace(/^-(.*)$/mg, '<span style="background-color:rgba(150,0,0,0.5)">-$1</span>')
  // sylyableDiff = sylyableDiff.replace(/^\+(.*)$/mg, '<span style="background-color:rgba(0,150,0,0.5)">+$1</span>')
  // styledConsoleLog(sylyableDiff)
}

export async function commitEpochToGit (filepath: string, data) {
  const gitInstance = await getGit()
  await fs.promises.writeFile(`/Epochs/${filepath}`, data)
  await gitInstance.add({ fs, dir: '/Epochs', filepath })
  const sha = await gitInstance.commit({ // eslint-disable-line @typescript-eslint/no-unused-vars
    fs,
    dir: '/Epochs',
    author: {
      name: 'Mr. Test',
      email: 'mrtest@example.com',
    },
    message: `Added ${filepath}`,
  })

  const commits = await gitInstance.log({
    fs,
    dir: '/Epochs',
    depth: 4,
  })

  console.log('git commits', commits)

  const files = await gitInstance.listFiles({ fs, dir: '/Epochs' })
  console.log('git files', files)
  const lastFilename = files[files.length - 1]

  const commitOid = await git.resolveRef({ fs, dir: '/Epochs', ref: defaultBranch })
  console.log('git commitOid', commitOid)
  const { blob } = await git.readBlob({
    fs,
    dir: '/Epochs',
    oid: commitOid,
    filepath: lastFilename,
  })
  console.log('git blob', blob, Buffer)
  console.log('git blob string', globalThis.Buffer.Buffer.from(blob).toString('utf8'))
}
// async function getFileStateChanges (commitHash1, commitHash2, dir) {
//   return await git.walk({
//     fs,
//     dir,
//     trees: [git.TREE({ ref: commitHash1 }), git.TREE({ ref: commitHash2 })],
//     async map (filepath, [A, B]) {
//       // ignore directories
//       if (filepath === '.') {
//         return
//       }
//       if ((await A.type()) === 'tree' || (await B.type()) === 'tree') {
//         return
//       }

//       // generate ids
//       const Aoid = await A.oid()
//       const Boid = await B.oid()

//       // determine modification type
//       let type = 'equal'
//       if (Aoid !== Boid) {
//         type = 'modify'
//       }
//       if (Aoid === undefined) {
//         type = 'add'
//       }
//       if (Boid === undefined) {
//         type = 'remove'
//       }
//       if (Aoid === undefined && Boid === undefined) {
//         console.log('Something weird happened:')
//         console.log(A)
//         console.log(B)
//       }

//       return {
//         path: `/${filepath}`,
//         type,
//       }
//     },
//   })
// }
