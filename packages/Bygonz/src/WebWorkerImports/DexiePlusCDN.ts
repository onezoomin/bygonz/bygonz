// import { Dexie } from 'dexie'
let DexieCDN
export const getDexieFromCDN = async () => {
  if (DexieCDN) return DexieCDN
  DexieCDN = (await import('https://unpkg.com/dexie@3.2.1/dist/modern/dexie.mjs')).Dexie
  return DexieCDN
}

// TODO consider refactor up into bygonz
const defaultOptions = {
  conflictThresholds: {
    red: 60,
    yellow: 300,
  },
}

export type DexiePlusParams = [name: string, stores: Record<string, string>, mappings?: Record<string, any>, options?: typeof defaultOptions, version?: number]

export const getDexiePlusCDN = async () => {
  const Dexie = await getDexieFromCDN()
  class DexiePlusCDN extends Dexie {
    public workerApi
    public stores
    public mappings
    public options

    public onWorkerMsg = console.log

    protected doMappings () {
      // console.log('befor tables?', this)
      for (const eachTable of this.tables) {
        // console.log('before constructor table assignment', this[eachTable.name])
        this[eachTable.name] = eachTable // ensure there is a reference to each table "on" the db instance
        // console.log('after constructor table assignment', this[eachTable.name], this)

        if (this.mappings[eachTable.name]) {
          // console.log(this[eachTable.name], 'mapping', eachTable, 'to', mappings[eachTable.name])
          eachTable.mapToClass(this.mappings[eachTable.name])
        }
      }
      // console.log('after tables?', this)
    }

    constructor (name: string, stores: Record<string, string>, mappings: Record<string, any> = {}, options = defaultOptions, version = 1) {
      if (!name || !stores) throw new Error('DexiePlus requires params (name, {stores})')
      super(name)

      this.version(version).stores(stores)

      this.options = options
      this.stores = stores
      this.mappings = mappings
      // this.doMappings() // needs to be called in the final child class that extends BygonzDexie
    }
  }
  return DexiePlusCDN
}
