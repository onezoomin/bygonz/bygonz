import format from 'date-fns/format'
import fromUnixTime from 'date-fns/fromUnixTime'
import getMinutes from 'date-fns/getMinutes'
import set from 'date-fns/set'
import type { Table } from 'dexie'
import { createYamlLog } from './Epochs'
import { CompoundKeyNumStr, ModVM, Operations } from './Mods'
import { checkWorker, utcMsTs } from './Utils'

/**
 * exporting this so watch will work
 * and maybe it can be used for quick and dirty config
 */
export let endpoint = 'https://dghttp.zt.ax/graphql' // eslint-disable-line prefer-const

const optionsBase = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    // authorization: 'Bearer MY_TOKEN',
  },
}

export const castJsonModArray = (modishArray): ModVM[] => modishArray.map((eachMod) => {
  const cleanMod = {
    ...eachMod,
    log: JSON.parse(eachMod.log),
    forKey: JSON.parse(eachMod.forKey),
  }
  delete cleanMod.__typename
  return new ModVM(cleanMod)
})

export const fetchMods = async (since = 0): Promise<ModVM[]> => {
  // console.time('fetchMods took')
  const modQuery = {
    query: `query {
  queryMod(order: { asc: ts },filter: {
    ts: {
      gt: ${since}
    }
  }) {
    ts
    tableName
    forKey
    owner
    modifier
    op
    log
  }
}`,
  }
  const body = JSON.stringify(modQuery)
  const options = {
    ...optionsBase,
    body,
  }
  let returnArray: ModVM[] = []
  try {
    const response = await fetch(endpoint, options)
    console.log('mod results', response)
    const data = await response.json()
    console.log('mod data', data)
    if (data?.data?.queryMod) {
      const mods = data.data.queryMod as ModVM[] ?? []
      if (mods?.length) {
        console.log('mod results', mods)
      } else {
        console.log('no mods returned', response, data)
      }
      returnArray = castJsonModArray(mods)
    }
  } catch (e) {
    console.warn(e)
  }
  // console.timeEnd('fetchMods took')
  return returnArray
}

export const dgraphMod = async (modIsh: any, modJson = (new ModVM(modIsh)).forGql()) => {
  checkWorker('dgraphMod')

  let response
  console.time('sendMod dgraph')
  // console.log('mod json obj:\n', modIsh)

  const data = {
    query: `mutation ($mod: [AddModInput!]!) {
      addMod(input: $mod, upsert: true) {
        mod {
          key
          ts
          tableName 
          forKey
          owner 
          modifier 
          op
          log
        }
      }
    }`,
    variables: {
      mod: modJson,
    },
  }
  // console.log('add mod mutation :\n', data)
  const body = JSON.stringify(data)
  const options = {
    ...optionsBase,
    body,
  }
  try {
    response = await fetch(endpoint, options)
    if (response.errors) {
      console.error(response)
    } else {
      console.log(response)
    }
  } catch (e) {
    console.error(e)
  }
  console.timeEnd('sendMod dgraph')
}

export const applyMods = async (castModsVMarray, whichDB) => {
  // console.log('apply', castModsVMarray)
  checkWorker('applyMods')
  if (!whichDB || !castModsVMarray?.length) return console.warn('bailing out of applyMods', whichDB, castModsVMarray)

  const addMods: Record<string, ModVM[]> = {}
  const putMods: Record<string, ModVM[]> = {}
  const delMods: Record<string, CompoundKeyNumStr[]> = {}

  for (const eachMod of castModsVMarray) {
    if (eachMod.op === Operations.UPDATE) {
      if (!putMods[eachMod.tableName]) putMods[eachMod.tableName] = []
      putMods[eachMod.tableName].push(eachMod.log.obj)
    } else if (eachMod.op === Operations.CREATE) {
      if (!addMods[eachMod.tableName]) addMods[eachMod.tableName] = []
      addMods[eachMod.tableName].push(eachMod.log.obj)
    } else if (eachMod.op === Operations.DELETE) {
      // const { primaryKey } = downlevelTable.schema
      // forKey = primaryKey?.extractKey(myRequest.values[0])
      if (!delMods[eachMod.tableName]) delMods[eachMod.tableName] = []
      delMods[eachMod.tableName].push(eachMod.forKey) // TODO check on this
    } else {
      console.log('unknown op', eachMod)
    }
  }
  console.log('apply', castModsVMarray, { add: addMods }, { put: putMods }, { del: delMods })
  // hookState.isSuspended = true
  // const bulkPromises: PromiseExtended[] = []
  // try {
  //   for (const eachTabName in addMods) {
  //     console.log('creating via Mods', eachTabName, addMods[eachTabName])
  //     bulkPromises.push((whichDB[eachTabName] as Table)?.bulkPut(addMods[eachTabName]))
  //   }
  //   for (const eachTabName in putMods) {
  //     console.log('updating via Mods', eachTabName, putMods[eachTabName])
  //     bulkPromises.push((whichDB[eachTabName] as Table)?.bulkPut(putMods[eachTabName]))
  //   }
  //   for (const eachTabName in delMods) {
  //     console.log('deleting via Mods', eachTabName, delMods[eachTabName])
  //     bulkPromises.push((whichDB[eachTabName] as Table)?.bulkDelete(delMods[eachTabName]))
  //   }
  //   await all(bulkPromises)

  // bulk promises is cute, but order must be create, update, delete
  try {
    for (const eachTabName in addMods) {
      console.log('creating via Mods', eachTabName, addMods[eachTabName])
      await (whichDB[eachTabName] as Table)?.bulkPut(addMods[eachTabName])
    }
    for (const eachTabName in putMods) {
      console.log('updating via Mods', eachTabName, putMods[eachTabName])
      await (whichDB[eachTabName] as Table)?.bulkPut(putMods[eachTabName])
    }
    for (const eachTabName in delMods) {
      console.log('deleting via Mods', eachTabName, delMods[eachTabName])
      await (whichDB[eachTabName] as Table)?.bulkDelete(delMods[eachTabName])
    }
  } catch (e) {
    console.error('applying mods', e)
  }
  await createYamlLog()
}

const since = {
  ts: 0,
}
const pendingFetches: number[] = []
let pendingTimeout: any
export const fetchAndApplyMods = async (modDB, targetDB, forceSinceZero = false) => {
  const nownow = utcMsTs()
  if (pendingFetches.length > 0) return
  pendingFetches.push(nownow)
  const nowDate = fromUnixTime(nownow / 1000)
  const prevMinute = set(nowDate, { minutes: getMinutes(nowDate) - 1, seconds: 0, milliseconds: 0 }).getTime()
  const nextMinute = set(nowDate, { minutes: getMinutes(nowDate) + 1, seconds: 0, milliseconds: 0 }).getTime()
  const thisMinute = set(nowDate, { minutes: getMinutes(nowDate), seconds: 0, milliseconds: 0 }).getTime()

  let knownMods: ModVM[] = []
  let fetchedMods: ModVM[] = []

  knownMods = await modDB.Mods.where('ts').above(forceSinceZero ? 0 : since.ts).toArray() as ModVM[]
  fetchedMods = await fetchMods(forceSinceZero ? 0 : since.ts) // returns cast ModVM
  checkWorker('fetchAndApplyMods fetched,known:', fetchedMods.length, knownMods.length, format(thisMinute, 'H:mm:ss:SSS'))
  // console.log(fetchedMods?.length, 'mods fetched since :', format(since.ts, 'H:mm:ss:SSS'))
  since.ts = prevMinute
  // console.log('this:', format(thisMinute, 'H:mm:ss:SSS'), 'prev:', format(since.ts, 'H:mm:ss:SSS'))

  // do bulkPut - idempotent opLog merge
  if (fetchedMods?.length) {
    const modKeysKnown = knownMods.map((eachMod) => JSON.stringify(ModVM.getCompoundKey(eachMod)))
    const modKeysRemotelyKnown = fetchedMods.map((eachMod) => JSON.stringify(ModVM.getCompoundKey(eachMod)))

    const unknownMods = fetchedMods.filter((eachIncomingMod) => !modKeysKnown.includes(JSON.stringify(eachIncomingMod.id)))
    const modsUnknownRemotely = knownMods.filter((eachLocallyKnownMod) => !modKeysRemotelyKnown.includes(JSON.stringify(eachLocallyKnownMod.id)))

    if (unknownMods.length) {
      console.log('modKeysKnown', modKeysKnown.length)
      console.log('unknown', unknownMods)
      await modDB.Mods.bulkPut(unknownMods)
      await applyMods(unknownMods, targetDB)
    } else {
      console.log('all mods already known', modKeysKnown.length)
      await createYamlLog()
    }
    if (modsUnknownRemotely.length) {
      console.log('unknown remotely', modsUnknownRemotely)
      for (const eachModToBeReported of modsUnknownRemotely) {
        void dgraphMod(eachModToBeReported) // TODO bulkPut
      }
    }
  }
  setTimeout(() => {
    void findAndFlagConflicts(modDB, targetDB)
  })
  console.log('fetchAndApply took', utcMsTs() - (pendingFetches.pop() ?? 0))

  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  if (!pendingTimeout) {
    pendingTimeout = setTimeout(async () => {
      await fetchAndApplyMods(modDB, targetDB)
      void createYamlLog(true)
      pendingTimeout = undefined
    }, nextMinute - nownow)
  }
}
// TODO revisit exampleCustomHandler
// const exampleCustomHandler = (allModsForKey: ModVM[]) => {
//   defer(() => {
//     const customFlags = []
//     // console.log('exampleCustomHandler', allModsForKey)
//     for (const eachMod of allModsForKey) {

//     }
//   })
// }

export const findAndFlagConflicts = async (modDB, targetDB, forceSinceZero = false) => {
  const st = performance.now() // utcMsTs()
  const { conflictThresholds: { red, yellow } } = targetDB.options // conflictHandlers: { customFlag = exampleCustomHandler },
  const allKnownMods = (await modDB.Mods.orderBy('forKey').toArray() ?? []) as ModVM[]

  const modsMappedbyForKey = new Map()
  interface flagObj { thisMod: ModVM, prevMod: ModVM, diffInSec: number }
  const redFlags: flagObj[] = []
  const yellowFlags: flagObj[] = []

  let prevKey = ''
  let prevMod
  for (const thisMod of allKnownMods) {
    const thisKey = thisMod.gqlForKey
    const thisModArray = modsMappedbyForKey.get(thisKey) ?? []
    if (prevKey) { // start testing on the second one
      if (thisKey !== prevKey) {
        // const prevModArray = modsMappedbyForKey.get(prevKey)
        // if (prevModArray.length > 1) exampleCustomHandler(prevModArray) // spin off a call to each custom conflictHandler as soon as the loop finishes
      } else if (thisMod.tableName === prevMod.tableName && thisMod.modifier !== prevMod.modifier) { // && thisMod.modifier !== prevMod.modifier
        const diffInSec = (thisMod.ts - prevMod.ts) * 0.001
        if (diffInSec < red) {
          // console.log('found red flag', thisMod, prevMod)
          redFlags.push({ thisMod, prevMod, diffInSec })
        } else if (diffInSec < yellow) {
          // console.log('found yellow flag', thisMod, prevMod)
          yellowFlags.push({ thisMod, prevMod, diffInSec })
        }
      }
    }
    // console.log(thisModArray)
    thisModArray.push(thisMod)
    modsMappedbyForKey.set(thisKey, thisModArray)
    prevKey = thisKey
    prevMod = thisMod
  }
  console.log('took', performance.now() - st, 'all mods mapped by key', modsMappedbyForKey, { yellowFlags, redFlags })
}
