// import resolve from '@rollup/plugin-node-resolve'
import path from 'path'
import { defineConfig } from 'vite'
// import worker, { pluginHelper } from 'vite-plugin-worker'
export default defineConfig({
  // optimizeDeps: {
  //   include: ['dexie'],

  // },
  // plugins: [
  //   pluginHelper(),
  //   worker({}),
  // ],
  worker: {
    format: 'es',
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      // inlineDynamicImports: true, // didn't help infinite loop
      // external: [...Object.keys(peerDependencies), ...Object.keys(devDependencies)],
    },
  },
  resolve: {
    // alias: {
      //   dedupe: [...Object.keys(peerDependencies), ...Object.keys(devDependencies)],
    //   process: 'process/browser',
    //   stream: 'stream-browserify',
    //   zlib: 'browserify-zlib',
    //   util: 'util',
    //   react: 'preact/compat',
    //   'react-dom': 'preact/compat',
    // },
    alias: [
      { find: 'react', replacement: 'preact/compat' },
      { find: 'react-dom', replacement: 'preact/compat' },
    ],
  },
  build: {
    target: 'esnext',
    // commonjsOptions: {
    //   // transformMixedEsModules: true,
    //   dynamicRequireTargets: [
    //     'src/BygonzWebWorker.ts',
    //   ],
    // },
    lib: {
      formats: ['es'],
      entry: path.resolve(__dirname, 'src/index.ts'),
      name: 'dexie-preact-hooks',
      fileName: (format) => `dexie-preact-hooks.${format}.js`,
    },
    sourcemap: true,
    rollupOptions: {
      
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      // inlineDynamicImports: true, // didn't help infinite loop
      // external: [...Object.keys(peerDependencies), ...Object.keys(devDependencies)],
      // plugins: [resolve()],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          dexie: 'Dexie',
        },
      },
    },
  },
  // define: {
  //   'process.env': process?.env || { isDev: import.meta.env.DEV }, // needed in addition to nodePolyfills
  //   // 'globalThis.Buffer': Buffer, // maybe needed in addition to nodePolyfills
  //   // Buffer, // maybe needed in addition to nodePolyfills
  // },
})
