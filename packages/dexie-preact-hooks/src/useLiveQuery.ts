import { liveQuery } from 'dexie';
import { useObservable } from './useObservable';
// const { liveQuery } = await import('https://unpkg.com/dexie@3.2.1/dist/modern/dexie.mjs')

export function useLiveQuery<T>(
  querier: () => Promise<T> | T,
  deps?: any[]
): T | undefined;
export function useLiveQuery<T, TDefault>(
  querier: () => Promise<T> | T,
  deps: any[],
  defaultResult: TDefault
): T | TDefault;
export function useLiveQuery<T, TDefault>(
  querier: () => Promise<T> | T,
  deps?: any[],
  defaultResult?: TDefault
): T | TDefault {
  return useObservable(
    () => liveQuery(querier),
    deps || [],
    defaultResult as TDefault
  );
}

export const DEXIE_STORAGE_MUTATED_EVENT_NAME = 'storagemutated' as 'storagemutated'
export const STORAGE_MUTATED_DOM_EVENT_NAME = 'x-storagemutated-1'; // x-storagemutated-1 "gets through" to the DOM but not storagemutated
export const getBrocastChannelSubscribedTo = (handler) => { 
  const bc = new BroadcastChannel(STORAGE_MUTATED_DOM_EVENT_NAME)
  bc.onmessage = handler
  return bc
}


    // targetDB.on('storagemutated', (ev) => {
    //   console.log('storagemutated', ev)
    // })

    
