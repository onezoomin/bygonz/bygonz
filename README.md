# Bygonz

Remember everything, until you don't want to.

## Offline First History Tracking

- Extends Dexie with modification History
- Syncronizes modifications via Dgraph
  - Syncronizes state via modification log

  ![Bygonz_whitebg](/uploads/2d9f37d3979937e8bd39787927e2443c/Bygonz_whitebg.png)

